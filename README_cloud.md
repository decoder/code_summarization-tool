decoder-code-summarization
==============================

Machine Learning Model. Extraction of information from source code and the automatic generation of summaries to describe the goal of the different methods.

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

# HOW-TOs

## How to run in local from source code 

0. Prerequisites:
- DECODER source code.
- Docker: https://docs.docker.com/engine/install/.
- Access to the internet to download data and dependencies.

1. Navigate to decoder-code-summarization/ folder and generate DECODER Docker images:
```
sudo docker build -f ./docker/Dockerfile -t decoder/transformer_training:latest .
```

2. Train the model:
```
sudo docker run -it --rm decoder/transformer_training:latest c use_case 2
```
Where:
   * First parameter (c) represents the use case data to use. Possible options are c and java.
   * Second parameter (use_case) represents the amount of data to use. Posible options are use_case (use data coming only from use cases) or augmented (use also augmented datasets).
   * Third parameter (2) represents the number of optimization iterations to use. Any integer greater than 0 is valid.


## Training in Amazon EC2

### Infrastructure setup
To train the transformers using the cloud the following Amazon services are needed:
1. Amazon Elastic Container Registry (ECR): Container registry in which to upload the Docker images that will be executed.
   * Name: code_summarization_transformers
   * URI: 670194352001.dkr.ecr.eu-west-3.amazonaws.com/code_summarization_transformers

2. Amazon Simple Storage Service (S3): Service to store the output of the execution, in this case the vocabulary dictionaries of the transformers, the best model and its hyperparameters.
   * Name of the bucket: decoder-transformer-training

3. Amazon Elastic Compute Cloud (EC2): Server in which the training will take place:
   * Instance type: g4dn.2xlarge (single GPU virtual machine with 32 GB of memory and 225 GB storage).
   * AMI 
   * Public DNS: ec2-13-36-168-21.eu-west-3.compute.amazonaws.com
   * The machine has already been configured to install Docker client and AWCLI with the following commands (after connecting via ssh):
       * sudo apt update
       * sudo apt install apt-transport-https ca-certificates curl software-properties-common
       * curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
       * sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
       * sudo apt update 
       * sudo apt install docker-ce
       * sudo apt  install awscli


### How to build images for uploading to Amazon ECR
**Important**: all commands below should be run from project root folder on the local machine (/decoder_code_summarization, branch amazon).
```
sudo docker build -f ./docker/Dockerfile -t 670194352001.dkr.ecr.eu-west-3.amazonaws.com/code_summarization_transformers:latest .
```

### How to push and pull images to/from ECR
**Important**: First you need to authenticate aws and docker clients.

0. Prerequisites: 
    - Install aws client: $ sudo apt install awscli
    - Install Docker client: $ sudo apt install docker.io
    - Generate AWS access KEY ID and secret Access KEY: https://console.aws.amazon.com/iam/home#/security_credentials (these are already in the folder resources/amazon from the root of the repo).

1. Set Amazon credentials to aws client (this step should be done only once):
```
aws configure
```

It will ask for: 
    - Access key ID
    - Secret access key
    - region: set to "eu-west-3"

2. Login Docker client:
```
sudo $(aws ecr get-login --registry-ids 670194352001 --no-include-email)
```

You should get a "Login Succeeded" message.

3. Push existing image to ECR:
```
sudo docker push 670194352001.dkr.ecr.eu-west-3.amazonaws.com/code_summarization_transformers:latest
```

4. Once an image is pushed to the registry it can be pulled from any machine with the following command:
```
sudo docker pull 670194352001.dkr.ecr.eu-west-3.amazonaws.com/code_summarization_transformers:latest
```
This has to be done once connected via ssh to the EC2 instance in order to run the docker image (see section below).


### How to connect to the EC2 instance via ssh and run the image
Go to the resources/amazon from the root of the repo and type:
```
ssh -i "decoder.pem" ubuntu@ec2-13-36-168-21.eu-west-3.compute.amazonaws.com
```

Please note pem file permissions must be changed in order to use the file:
```
sudo chmod 400 decoder.pem
```

These are the steps to follow:
1. Set Amazon credentials to aws client (you can skip this step if it has already been done before):
```
aws configure
```

It will ask for: 
    - Access key ID
    - Secret access key
    - region: set to "eu-west-3"

2. 2. Login Docker client:
```
sudo $(aws ecr get-login --registry-ids 670194352001 --no-include-email)
```

3. Pull the image from the ECR registry:
```
sudo docker pull 670194352001.dkr.ecr.eu-west-3.amazonaws.com/code_summarization_transformers:latest
```

4. Run the execution:
```
sudo docker run -it --name=decoder --rm 670194352001.dkr.ecr.eu-west-3.amazonaws.com/code_summarization_transformers:latest c use_case 50
```

Where:
   * c represents the use case, options are: c or java
   * use_case represents the data to load, options are: use_case (for only use case related data) or augmented (for training with use case data plus the augmented datasets)
   * 50 represents the number of iterations for bayesian optimization. In this case 50 models will be trained and the best one will be retained. 

5. To see the logs if you want to close the local terminal:
   * Connect to the EC2 instance via ssh as described in section above.
   * Execute 'sudo docker logs decoder'
