# decoder-code-summarization-development-environment

This is a generic development environment for the development of software components and machine learning models for code summarization.

## First steps

The docker environment comes without pre-install packages, the basic environment is created with the following command than only needs to be executed the first time the environment is setup:

```bash
conda env create -f environment.yml
```

Once the environment is created, it is preserved in between environment restarts, it can be activated as:

```bash
conda activate tensorflow-gpu
```

## Train model

In order to train the model with `hyperopt`:

```bash
python3 src/transformers/RUN_hyperopt.py <programming language> <dataset mode> <optimization iterations>
```

## Model inference

For inference:

```bash
python3 src/transformers/RUN_inference.py <programming language> <dataset mode> 
```

### Arguments meaning

`Programming language` can be:
1. Java
2. C

`Dataset mode` can be:
1. augmented: For training with data augmentation
2. use_case: With no data augmentation

`Optimization iterations` can be:
Integer for hyperopt optimizations

### Git LFS data

Be careful that the LFS files tracked are downloaded when you run the scripts. If they are not downloaded, you can download all with:
```bash
git lfs pull
```
from the root directory of this repo