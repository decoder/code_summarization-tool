import time
import os
import pandas as pd
import numpy as np
import sys
import datetime
import argparse
sys.path.append(".")
import tensorflow as tf
tf.get_logger().setLevel('ERROR')

from src.transformers.imports.transformer_modules import Transformer, CustomSchedule, create_padding_mask, create_look_ahead_mask, Encoder, Decoder, MultiHeadAttention
from src.transformers.imports.training_utils import  evaluate, seq2text, seq2summary, summarize, get_bleu_score  
from src.transformers.imports.training_utils import feature_extraction_inference
from src.transformers.imports.preprocessing_java import tokenization_java, import_dicts
from src.transformers.imports.preprocessing_c import tokenization_c, import_dicts

#os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

if tf.test.gpu_device_name():
    print('GPU found')
else:
    print("No GPU found")

parser = argparse.ArgumentParser()
parser.add_argument("language")
parser.add_argument("dataset")
args = parser.parse_args()

### Load training dicts
dict_path = 'transformer_best_model/dicts/'
token2idx, target_word_index, reverse_target_word_index = import_dicts(dict_path, 'token2idx_HyperOpt_transformer_'+args.language+'.json', 'y2idx_HyperOpt_transformer_'+args.language+'.json', 'idx2y_HyperOpt_transformer_'+args.language+'.json')
reverse_source_word_index = {v: k for k, v in token2idx.items()}


# ## Load transformer model
# Paths and names to load the inference models (encoder and decoder)
model_path = 'transformer_best_model/my_best_model'
loaded_model = tf.keras.models.load_model(model_path, custom_objects={'Transformer':Transformer, 'Encoder': Encoder, 'Decoder': Decoder, 'MultiHeadAttention': MultiHeadAttention})
#model_path = 'my_best_model'
#loaded_model = tf.keras.models.load_model(model_path)
print(loaded_model.summary())


# ## Process source files for inference
#Load code files
code_files = [
'data/same_format/OpenCV/028.c',
'data/same_format/OpenCV/231.c',
'data/same_format/OpenCV/483.c',
'data/same_format/Drivers/e1000_read_flash_data_ich8lan.c',
'data/same_format/Drivers/e1000e_get_cable_length_m88.c',
]

filenames = [x.split('/')[-1] for x in code_files]

#apply pre-processing
if args.language == 'c':
    token_list = tokenization_c(code_files)
else:
    token_list = tokenization_java(code_files)

#apply feature extraction
max_len_code = 200
max_len_summary = 15
X = feature_extraction_inference(token2idx, token_list, max_len_code)

print('Total percentage of unknown tokens: {}% (being {}% elements set to PAD)'.format(np.count_nonzero(X == 1 )/X.size , np.count_nonzero(X == 0)/X.size))

def make_predictions(X, model, target_word_index, reverse_target_word_index, token2idx, reverse_source_word_index, max_len_code, max_len_summary):

    target, decoded = [], []
    for index in range(len(X)):
        print('Predicting input snippet: ',filenames[index])
        print("Input snippet:\n",seq2text(X[index], reverse_source_word_index))
        sentence = summarize(seq2text(X[index], reverse_source_word_index), model, token2idx, target_word_index, max_len_code, max_len_summary, reverse_target_word_index)
        print()
        print('Predicted summary:\n',sentence)
        decoded.append(sentence)
        print()
    return decoded

#get predictions
decoded_sentences = make_predictions(X,loaded_model, target_word_index, reverse_target_word_index, token2idx, reverse_source_word_index, max_len_code, max_len_summary)


