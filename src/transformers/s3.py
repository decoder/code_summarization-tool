import boto3
import os
import csv
import shutil
from botocore.exceptions import NoCredentialsError
from datetime import datetime

BUCKET_NAME = 'decoder-transformer-training'
S3_CREDENTIALS_CSV_FILE = 'src/transformers/credentials/s3_user_credentials.csv'


def __read_s3_user_credentials(csv_credentials_file):
    """
    Function to get credentials from CSV key file downloaded from AWS web console

    Parameters
    ----------
    csv_credentials_file: String
        Full path file
    """
    with open(csv_credentials_file, newline='') as f:
        csv_reader = csv.reader(f)
        next(csv_reader)  # Skip headers
        first_line = next(csv_reader)
        return first_line[2], first_line[3]


def upload_folder_to_s3(path):
    """
        Function to send folder content to AWS S3 within a timestamp parent folder.
        Example: if you have /dir1/dir2/file.txt and path = 'dir1', it will create:
                 /20200813_120000/dir2/file.txt

        Parameters
        ----------
        path: String
            Folder path
        """
    access_key, secret_key = __read_s3_user_credentials(S3_CREDENTIALS_CSV_FILE)
    session = boto3.Session(
        aws_access_key_id=access_key,
        aws_secret_access_key=secret_key,
        region_name='eu-west-3'
    )
    s3 = session.resource('s3')
    bucket = s3.Bucket(BUCKET_NAME)
    timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")

    for subdir, dirs, files in os.walk(path):
        for file in files:
            full_path = os.path.join(subdir, file)
            with open(full_path, 'rb') as data:
                bucket.put_object(Key= os.path.join(timestamp, full_path[len(path)+1:]), Body=data)
    return 'https://s3.console.aws.amazon.com/s3/buckets/{}/{}/?region=eu-west-3&tab=overview'.format(BUCKET_NAME, timestamp)


if __name__ == "__main__":
    print(upload_folder_to_s3('l2f_deps'))
