import os
import sys
import pandas as pd
import re
import pygments
from pygments.lexers import JavaLexer, CppLexer
import nltk
nltk.download('punkt')
import time
import json

from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer

##from https://github.com/wasiahmad/NeuralCodeSum ('A Transformer-based Approach for Source Code Summarization')
from src.transformers.imports.tokenizer import CodeTokenizer     
    

def import_files(code_path, code_pattern, annotations_file):
    
    #assume annotations file is ordered by filename
    annotations_df = pd.read_csv(annotations_file).drop('Unnamed: 0', axis = 1)
    annotations = annotations_df.sort_values(by=['filename'])['summary'].values
    annotations = [re.sub('\s+',' ', x).strip() for x in annotations]  #discard \n
    
    # Source code files
    code_files = []
    for root, dirs, files in os.walk(code_path):
        files = [f for f in files if not f[0] == '.'] # Filter hidden files (from ipynb_checkpoints)
        dirs[:] = [d for d in dirs if not d[0] == '.'] # Filter hidden directories
        for file in files:
            if file.endswith(code_pattern):
                code_files.append(os.path.join(root, file))    
    
    #sort files so that they match with annotations
    code_files.sort(key = lambda x: x.split('/')[-1])
    
    print('Number of files in source code: ', len(code_files))
    print('Number of annotations: ', len(annotations))
    print()

    return annotations, code_files


def read_files_and_tokenize(code_files, language):

    if language=='java':
        lexer = JavaLexer()
    elif language=='c':
        lexer = CppLexer()
        
    list_tokens = []
    for file in code_files:
        snippet = open(file, "r").read().rstrip() # Read every code file into a string
        result = pygments.lex(snippet, lexer)
        list_tokens.append(list(result))

    return list_tokens
    

def split_identifier_names(list_tokens):  # split by snake case, camel case, etc...
    
    #define code tokenizer
    code_tokenizer = CodeTokenizer()
    
    final_token_list = []

    for code_file in list_tokens:
        inside = []
        for tup in code_file:

            #splitting comment and literals with word tokenization + identifiers splitting
            if str(tup[0]).startswith('Token.Comment') or str(tup[0]).startswith('Token.Literal'):

                if len(list(nltk.word_tokenize(tup[1]))) >1:

                    for split in list(nltk.word_tokenize(tup[1])):
                        splitted = ((tup[0], re.sub(r'\W+', '', split)))

                        if len(list(code_tokenizer.tokenize(splitted[1])))>1:
                            for split in list(code_tokenizer.tokenize(splitted[1])):
                                splitted = ((tup[0], split))
                                inside.append(splitted)
                        else:
                            inside.append(splitted)    

            #apply identifiers splitting to names             
            elif str(tup[0]).startswith('Token.Name'):

                if len(list(code_tokenizer.tokenize(tup[1]))) >1:

                    for split in list(code_tokenizer.tokenize(tup[1])):
                        splitted = ((tup[0], split))
                        inside.append(splitted)
                else:
                    inside.append((tup))
                    
            # Filter blank spaces, carriage returns and puntuations       
            elif str(tup[0])=='Token.Text' or str(tup[1])=='\n' or str(tup[1])=='' or str(tup[0])=='Token.Punctuation':
                continue
                
            else:
                inside.append((tup))

        final_token_list.append(inside)

    return final_token_list


def final_preproc(tuple_tokens):
    list_tokens = []
    for file in tuple_tokens:
        file_tokens = []
        for tup in file:
            # Filtering empty tokens, puntuation
            if str(tup[1]) != '':
                if str(tup[0]) != 'Token.Punctuation':
                    if str(tup[0]) == 'Token.Literal.Number.Integer': # Substitute integer tokens by 'INT'
                        file_tokens.append('INT')
                      
                    elif str(tup[0]) == 'Token.Literal.Number.Float': # Substitute float tokens by 'FLOAT'
                        file_tokens.append('FLOAT')
                        
                    else:
                        file_tokens.append(str(tup[1]).lower())
        #print('\nTokenized file after preprocessing: ', file_tokens)
        list_tokens.append(file_tokens)
    return list_tokens
  

def tokenization_java(code_files):    
    
    start = time.time()
    print('Starting tokenization of source code files...')
    tuple_tokens = read_files_and_tokenize(code_files, 'java')
    tuple_tokens = split_identifier_names(tuple_tokens)
    print('Tokenization ready!')
    print('Tokenization time: %0.4f seconds' %(time.time()-start))
    print()

    start = time.time()
    print('Starting preproc of source code files...')
    list_tokens = final_preproc(tuple_tokens)
    print('Preprocessing ready!')
    print('Preprocessing time: %0.4f seconds' %(time.time()-start))
    print()
    
    
    return list_tokens


def import_dicts(dicts_path, token2idx_file, word2idx_file, idx2word_file):

    # Load input and output dictionaries
    # Input
    #token2idx_file = 'token2idx_java.json'
    with open(dicts_path+token2idx_file) as json_file:
        token2idx = json.load(json_file)

    # Output
    #word2idx_file = 'y2idx_java.json'
    with open(dicts_path+word2idx_file) as json_file:
        target_word_index = json.load(json_file)

    #idx2word_file = 'idx2y_java.json'
    with open(dicts_path+idx2word_file) as json_file:
        reverse_target_word_index = json.load(json_file)

    
    reverse_target_word_index = {int(w):i for w, i in reverse_target_word_index.items()}

    return token2idx, target_word_index, reverse_target_word_index


