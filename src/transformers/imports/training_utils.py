from numpy.random import seed
seed(1)

import tensorflow as tf
from tensorflow import keras
#from tensorflow.keras import layers

import os
from glob import glob
import sys
import time
import re
import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer

##from https://github.com/wasiahmad/NeuralCodeSum ('A Transformer-based Approach for Source Code Summarization')
from src.transformers.imports.tokenizer import CodeTokenizer    

from nltk.translate.bleu_score import sentence_bleu
from src.transformers.imports.transformer_modules import Transformer, CustomSchedule, create_padding_mask, create_look_ahead_mask

    
def feature_extraction_training(max_len_text, max_len_summary, list_tokens, summaries, use_case):
    
    if use_case=='java':
        count_thres = 3
    elif use_case=='c':
        count_thres = 2
        
    #define code tokenizer
    code_tokenizer = CodeTokenizer()
    
    summaries = ['<start> '+' '.join(code_tokenizer.tokenize(x))+' <end>' for x in summaries]
    #summaries = ['<start> '+x+' <end>' for x in summaries]
    
    X_tr, X_te, y_tr, y_te = train_test_split(list_tokens, summaries, test_size=0.2, random_state=42)
    X_val, X_te, y_val, y_te = train_test_split(X_te, y_te, test_size=0.5, random_state=42)
    
    print('\nTraining set size:', len(X_tr))
    print('Validation set size:', len(X_val))
    print('Test set size:', len(X_te))
    
    file_tokens_tr = [' '.join(x) for x in X_tr]
    file_tokens_val = [' '.join(x) for x in X_val]
    file_tokens_te = [' '.join(x) for x in X_te]

    #preparing a tokenizer for summary on training data 
    x_tokenizer = Tokenizer(oov_token = 'UNK', filters=' ')
    x_tokenizer.fit_on_texts(file_tokens_tr)

    #filtered x_tokenizer by deleting items based on word counts
    low_count_words = [w for w,c in x_tokenizer.word_counts.items() if c < count_thres]
    #print(x_tokenizer.texts_to_sequences(file_tokens_tr[0]))
    for w in low_count_words:
        #print(x_tokenizer.word_index[w])
        del x_tokenizer.word_index[w]
        del x_tokenizer.word_docs[w]
        del x_tokenizer.word_counts[w]    
        
    X_tr = x_tokenizer.texts_to_sequences(file_tokens_tr) 
    X_val = x_tokenizer.texts_to_sequences(file_tokens_val) 
    X_te = x_tokenizer.texts_to_sequences(file_tokens_te) 

    x_tokenizer.word_index['PAD'] = 0
    x_tokenizer.index_word[0] = 'PAD'
    
    X_tr = pad_sequences(maxlen=max_len_text, sequences=X_tr, padding='post', truncating = 'post', value=x_tokenizer.word_index['PAD']) # For sentences shorter than max_len 
    X_val = pad_sequences(maxlen=max_len_text, sequences=X_val, padding='post', truncating = 'post', value=x_tokenizer.word_index['PAD']) # For sentences shorter than max_len 
    X_te = pad_sequences(maxlen=max_len_text, sequences=X_te, padding='post', truncating = 'post',value=x_tokenizer.word_index['PAD']) # For sentences shorter than max_len 
    
    #preparing a tokenizer for summary on training data 
    y_tokenizer = Tokenizer(oov_token = 'UNK', filters='!"#$%&()*+,-./:;=?@[\\]^_`{|}~\t\n')
    y_tokenizer.fit_on_texts(y_tr)

    #convert summary sequences into integer sequences
    y_tr = y_tokenizer.texts_to_sequences(y_tr) 
    y_val = y_tokenizer.texts_to_sequences(y_val) 
    y_te = y_tokenizer.texts_to_sequences(y_te) 

    y_tokenizer.word_index['PAD'] = 0
    y_tokenizer.index_word[0] = 'PAD'
    
    #padding zero upto maximum length
    y_tr = pad_sequences(y_tr, maxlen=max_len_summary, padding='post', truncating ='post',value=y_tokenizer.word_index['PAD'])
    y_val = pad_sequences(y_val, maxlen=max_len_summary, padding='post', truncating ='post',value=y_tokenizer.word_index['PAD'])
    y_te = pad_sequences(y_te, maxlen=max_len_summary, padding='post', truncating ='post',value=y_tokenizer.word_index['PAD'])
    
    x_voc_size = len(x_tokenizer.word_index) 
    print('\nInput vocabulary size',x_voc_size)
    y_voc_size = len(y_tokenizer.word_index) 
    print('Output vocabulary size',y_voc_size)
    
    return X_tr, X_val, X_te, y_tr, y_val, y_te, x_tokenizer, y_tokenizer 

def feature_extraction_inference(token2idx, list_tokens, max_len_text):
    
    # Vectorize sentences
    X = [[token2idx[t] if t in token2idx else 1 for t in file] for file in list_tokens] #1 is label for 'UNK'
    # Padding
    X = pad_sequences(X, maxlen=max_len_text, padding='post', truncating = 'post', value=token2idx['PAD'])
    return X


def create_masks(inp, tar):
    enc_padding_mask = create_padding_mask(inp)
    dec_padding_mask = create_padding_mask(inp)

    look_ahead_mask = create_look_ahead_mask(tf.shape(tar)[1])
    dec_target_padding_mask = create_padding_mask(tar)
    combined_mask = tf.maximum(dec_target_padding_mask, look_ahead_mask)

    return enc_padding_mask, combined_mask, dec_padding_mask

def evaluate(input_document, model, token2idx, target_word_index, encoder_maxlen, decoder_maxlen):
    
    input_document = [token2idx[str(x)] if x in token2idx.keys() else 1  for x in input_document.split(' ')]
    input_document = tf.keras.preprocessing.sequence.pad_sequences([input_document], maxlen=encoder_maxlen, padding='post', truncating='post')

    encoder_input = tf.expand_dims(input_document[0], 0)

    decoder_input = [target_word_index["<start>"]]
    output = tf.expand_dims(decoder_input, 0)

    for i in range(decoder_maxlen-1): 
        enc_padding_mask, combined_mask, dec_padding_mask = create_masks(encoder_input, output)

        this = [encoder_input, output, enc_padding_mask, combined_mask, dec_padding_mask]

        predictions, attention_weights = model(
                this, False
            )

        predictions = predictions[: ,-1:, :]
        predicted_id = tf.cast(tf.argmax(predictions, axis=-1), tf.int32)

        if predicted_id == target_word_index["<end>"]:
            return tf.squeeze(output, axis=0), attention_weights

        output = tf.concat([output, predicted_id], axis=-1)

    return tf.squeeze(output, axis=0), attention_weights

def summarize(input_document, model, token2idx, target_word_index, encoder_maxlen, decoder_maxlen, reverse_target_word_index):
        # not considering attention weights for now, can be used to plot attention heatmaps in the future
    summarized = evaluate(input_document, model, token2idx, target_word_index, encoder_maxlen, decoder_maxlen)[0]
    summarized = np.expand_dims(summarized[1:], 0)  # not printing <go> token
    #print(summarized)
    #print(y_tokenizer.sequences_to_texts(summarized)[0])
    #print(' '.join([reverse_target_word_index[y] for y in summarized[0]]))
    return ' '.join([reverse_target_word_index[y] for y in summarized[0]])

def seq2summary(input_seq, target_word_index, reverse_target_word_index):
    newString=''
    for i in input_seq:
        if((i!=0 and i!=target_word_index['<start>']) and i!=target_word_index['<end>']):
            newString=newString+reverse_target_word_index[i]+' '
    return newString

def seq2text(input_seq, reverse_source_word_index):
    newString=''
    for i in input_seq:
        if(i!=0):
            newString=newString+reverse_source_word_index[i]+' '
    return newString

def make_predictions(X, y, model, target_word_index, reverse_target_word_index, token2idx, reverse_source_word_index, encoder_maxlen, decoder_maxlen):

    target = []
    decoded = []

    for index in range(len(X)):
        #print(index)
        #print("Snippet:",seq2text(X[index]))
        #print("Original summary:",seq2summary(y[index]))
        #print('Predicted summary: ',summarize(seq2text(X[index])))
        target.append(seq2summary(y[index],target_word_index, reverse_target_word_index))
        decoded.append(summarize(seq2text(X[index], reverse_source_word_index), model, token2idx, target_word_index, encoder_maxlen, decoder_maxlen, reverse_target_word_index))
        #print('\n')

    return decoded, target

def get_bleu_score(decoded, target):
        
    bleu_1, bleu_2, bleu_3, bleu_4 = [], [], [], []

    for i,(original, predicted) in enumerate(zip(target, decoded)):
        ys = [y for y in original.split(' ') if y!='']
        preds = [x for x in str(predicted).replace('PAD', '').split(' ') if x!='']
        #print(ys)
        #print(preds)
        #score.append(sentence_bleu([y for y in o.split(' ') if y!=''],[x for x in str(p).replace('PAD', '').split(' ') if x != '']))
        bleu_1.append(sentence_bleu([ys], preds, weights=(1,)))
        bleu_2.append(sentence_bleu([ys], preds, weights=(.5,.5)))
        bleu_3.append(sentence_bleu([ys], preds, weights=(.33,.33 ,.33)))
        bleu_4.append(sentence_bleu([ys], preds))

    return bleu_1, bleu_2, bleu_3, bleu_4

def Callback_EarlyStopping(MetricList, min_delta, patience, mode='min'):
    #No early stopping for the first patience epochs 
    if len(MetricList) <= patience:
        return False
    
    min_delta = abs(min_delta)
    if mode == 'min':
        min_delta *= -1
    else:
        min_delta *= 1
    
    #last patience epochs 
    last_patience_epochs = [x + min_delta for x in MetricList[::-1][1:patience + 1]]
    current_metric = MetricList[::-1][0]
    #print(last_patience_epochs)
    print(min(last_patience_epochs))
    print(current_metric)
    
    if mode == 'min':
        if current_metric >= min(last_patience_epochs):
            print(f'Metric did not decrease for the last {patience} epochs.')
            return True
        else:
            return False
    else:
        if current_metric <= min(last_patience_epochs):
            print(f'Metric did not increase for the last {patience} epochs.')
            return True
        else:
            return False  



def feature_extraction_training_augmented(max_len_text, max_len_summary, X_tr, y_tr, X_val, y_val, X_te, y_te,use_case):
    
    if use_case=='java':
        count_thres_x = 5
        count_thres_y = 5
        
    elif use_case=='c':
        count_thres_x = 10
        count_thres_y = 5
    
    print('\nTraining set size:', len(X_tr))
    print('Validation set size:', len(X_val))
    print('Test set size:', len(X_te))
    
    file_tokens_tr = [' '.join(x) for x in X_tr]
    file_tokens_val = [' '.join(x) for x in X_val]
    file_tokens_te = [' '.join(x) for x in X_te]

    #preparing a tokenizer for summary on training data 
    x_tokenizer = Tokenizer(oov_token = 'UNK', filters=' ')
    x_tokenizer.fit_on_texts(file_tokens_tr)

    #filtered x_tokenizer by deleting items based on word counts
    low_count_words = [w for w,c in x_tokenizer.word_counts.items() if c < count_thres_x]
    #print(x_tokenizer.texts_to_sequences(file_tokens_tr[0]))
    for w in low_count_words:
        #print(x_tokenizer.word_index[w])
        del x_tokenizer.word_index[w]
        del x_tokenizer.word_docs[w]
        del x_tokenizer.word_counts[w]    
        
    X_tr = x_tokenizer.texts_to_sequences(file_tokens_tr) 
    X_val = x_tokenizer.texts_to_sequences(file_tokens_val) 
    X_te = x_tokenizer.texts_to_sequences(file_tokens_te) 

    x_tokenizer.word_index['PAD'] = 0
    x_tokenizer.index_word[0] = 'PAD'
    
    X_tr = pad_sequences(maxlen=max_len_text, sequences=X_tr, padding='post', truncating = 'post', value=x_tokenizer.word_index['PAD']) # For sentences shorter than max_len 
    X_val = pad_sequences(maxlen=max_len_text, sequences=X_val, padding='post', truncating = 'post', value=x_tokenizer.word_index['PAD']) # For sentences shorter than max_len 
    X_te = pad_sequences(maxlen=max_len_text, sequences=X_te, padding='post', truncating = 'post',value=x_tokenizer.word_index['PAD']) # For sentences shorter than max_len 
    
    #preparing a tokenizer for summary on training data 
    y_tokenizer = Tokenizer(oov_token = 'UNK', filters='!"#$%&()*+,-./:;=?@[\\]^_`{|}~\t\n')
    y_tokenizer.fit_on_texts(y_tr)

    #filtered x_tokenizer by deleting items based on word counts
    low_count_words = [w for w,c in y_tokenizer.word_counts.items() if c < count_thres_y]
    #print(x_tokenizer.texts_to_sequences(file_tokens_tr[0]))
    for w in low_count_words:
        #print(x_tokenizer.word_index[w])
        del y_tokenizer.word_index[w]
        del y_tokenizer.word_docs[w]
        del y_tokenizer.word_counts[w]   
        
    #convert summary sequences into integer sequences
    y_tr = y_tokenizer.texts_to_sequences(y_tr) 
    y_val = y_tokenizer.texts_to_sequences(y_val) 
    y_te = y_tokenizer.texts_to_sequences(y_te) 

    y_tokenizer.word_index['PAD'] = 0
    y_tokenizer.index_word[0] = 'PAD'
    
    #padding zero upto maximum length
    y_tr = pad_sequences(y_tr, maxlen=max_len_summary, padding='post', truncating ='post',value=y_tokenizer.word_index['PAD'])
    y_val = pad_sequences(y_val, maxlen=max_len_summary, padding='post', truncating ='post',value=y_tokenizer.word_index['PAD'])
    y_te = pad_sequences(y_te, maxlen=max_len_summary, padding='post', truncating ='post',value=y_tokenizer.word_index['PAD'])
    
    x_voc_size = len(x_tokenizer.word_index) 
    print('\nInput vocabulary size',x_voc_size)
    y_voc_size = len(y_tokenizer.word_index) 
    print('Output vocabulary size',y_voc_size)
    
    return X_tr, X_val, X_te, y_tr, y_val, y_te, x_tokenizer, y_tokenizer 
