import time
import os
import pandas as pd
import numpy as np
import datetime
import argparse
import tensorflow as tf
import random 
import string

import sys
sys.path.append("/home/jovyan/work/2021/DECODER/experiments/decoder-code-summarization")
#sys.path.append("/home/jovyan/work/2021/DECODER/experiments/java")


from sklearn.model_selection import train_test_split
from src.transformers.imports.transformer_modules import Transformer, CustomSchedule, create_padding_mask, create_look_ahead_mask
from hyperopt import Trials, STATUS_OK, tpe, fmin, hp

##from https://github.com/wasiahmad/NeuralCodeSum ('A Transformer-based Approach for Source Code Summarization')
from src.transformers.imports.tokenizer import CodeTokenizer    

#from training_utils import feature_extraction_training_AUGMENTED
#from training_utils import feature_extraction_training_augmented
from src.transformers.imports.training_utils import Callback_EarlyStopping
from src.transformers.imports.training_utils import  get_bleu_score, make_predictions
from src.transformers.s3 import upload_folder_to_s3

if tf.test.gpu_device_name():
    print('GPU found')
else:
    print("No GPU found")

parser = argparse.ArgumentParser()
parser.add_argument("language")
parser.add_argument("dataset")
parser.add_argument("iterations", default=50, type=int)
args = parser.parse_args()
iterations = args.iterations
 
#set files paths according to use cases
if args.language == 'java':
    print("\nJava use cases selected\n")
    code_path1 = 'data/same_format/myTHAI'
    annotations_file1 = 'data/same_format/myTHAI/myTHAI_annotations.csv'
    code_path2 = 'data/same_format/JAVA'
    annotations_file2 = 'data/same_format/JAVA/JAVA_annotations.csv'
    
else:
    print("\nC/C++ use cases selected\n")
    code_path1 = 'data/same_format/OpenCV'
    annotations_file1 = 'data/same_format/OpenCV/OpenCV_annotations.csv'
    code_path2 = 'data/same_format/Drivers'
    annotations_file2 = 'data/same_format/Drivers/Drivers_annotations.csv'

#import DECODER datasets
if args.language == 'c':
    from src.transformers.imports.preprocessing_c import import_files, tokenization_c
    annotations1, code_files1 = import_files(code_path1, '.'+args.language, annotations_file1)
    annotations2, code_files2 = import_files(code_path2, '.'+args.language, annotations_file2)

    print('OPENCV:')
    list_tokens1 = tokenization_c(code_files1)
    print('DRIVERS:')
    list_tokens2 = tokenization_c(code_files2)

if args.language == 'java':
    from src.transformers.imports.preprocessing_java import import_files, tokenization_java
    annotations1, code_files1 = import_files(code_path1, '.'+args.language, annotations_file1)
    annotations2, code_files2 = import_files(code_path2, '.'+args.language, annotations_file2)

    annotations1 = [y.replace('<code>', '').replace('</code>', '').replace('{@link','').replace('}', '') for y in annotations1]
    annotations1 = [y.replace('<p>', '').replace('<li>', '').replace('</li>', '') for y in annotations1]

    annotations2 = [y.replace('<code>', '').replace('</code>', '').replace('{@link','').replace('}', '') for y in annotations2]
    annotations2 = [y.replace('<p>', '').replace('<li>', '').replace('</li>', '') for y in annotations2]

    print('myTHAI:')
    list_tokens1 = tokenization_java(code_files1)
    print('JAVA:')
    list_tokens2 = tokenization_java(code_files2)


list_tokens_decoder = list_tokens1 + list_tokens2
print('DECODER source code files:',len(list_tokens_decoder))
summaries_decoder = annotations1 + annotations2
print('DECODER descriptions:',len(summaries_decoder))

if args.dataset == 'augmented':
    from src.transformers.imports.training_utils import feature_extraction_training_augmented
    #import augmented datasets (already cleaned and tokenized)
    if args.language == 'c':
        augmented = pd.read_csv('data/cleaned_C_Cpp_dataset.csv').drop('Unnamed: 0', axis = 1)
        print('\nC/C++ augmented dataset:')
        list_tokens_augmented = [str(x)[2:-3] for x in augmented['tokens'].values]
        list_tokens_augmented = [x.replace("'", '').split(', ') for x in list_tokens_augmented]
        print(list_tokens_augmented[0:10])
        summaries_augmented = augmented['summaries'].values


    if args.language == 'java':
        augmented = pd.read_csv('data/cleaned_DeepCom_dataset.csv').drop('Unnamed: 0', axis = 1).sample(frac=0.60)
        print('\nDeepComm:')
        list_tokens_augmented = augmented['code'].values
        list_tokens_augmented = [x.split(' ') for x in list_tokens_augmented]    
        summaries_augmented = augmented['summaries'].values

        summaries_augmented = [y.replace('<code>', '').replace('</code>', '').replace('{@link','').replace('}', '') for y in summaries_augmented]
        summaries_augmented = [y.replace('<p>', '').replace('<li>', '').replace('</li>', '') for y in summaries_augmented]


    print('Augmented source code files:',len(list_tokens_augmented))
    print('Augmented descriptions:',len(summaries_augmented))

    filenames_decoder = code_files1 + code_files2
    filenames_augmented = list(augmented.index.values)
    #all_filenames = filenames_decoder + filenames_augmented
    
    #filenames_tr, filenames_te = train_test_split(all_filenames, test_size=0.15, random_state=42)
    #filenames_val, filenames_te = train_test_split(filenames_te, test_size=0.5, random_state=42)

    filenames_train_decoder, filenames_test_decoder = train_test_split(filenames_decoder, test_size=0.15, random_state=42)
    filenames_valid_decoder, filenames_test_decoder = train_test_split(filenames_test_decoder, test_size=0.5, random_state=42)
    filenames_train_augmented, filenames_test_augmented = train_test_split(filenames_augmented, test_size=0.15, random_state=42)
    filenames_valid_augmented, filenames_test_augmented = train_test_split(filenames_test_augmented, test_size=0.5, random_state=42)
    
    filenames_train = filenames_train_decoder + filenames_train_augmented
    filenames_valid = filenames_valid_decoder + filenames_valid_augmented
    filenames_test = filenames_test_decoder + filenames_test_augmented
    
    print('\nTotal number of source code files:',len(list_tokens_decoder)+len(list_tokens_augmented))
    print('Total number of descriptions:',len(summaries_decoder)+ len(summaries_augmented))

    #define code tokenizer
    code_tokenizer = CodeTokenizer()

    summaries_augmented = ['<start> '+' '.join(code_tokenizer.tokenize(x))+' <end>' for x in summaries_augmented]
    summaries_decoder = ['<start> '+' '.join(code_tokenizer.tokenize(x))+' <end>' for x in summaries_decoder]

    X_tr_DECODER, X_te_DECODER, y_tr_DECODER, y_te_DECODER = train_test_split(list_tokens_decoder, summaries_decoder, test_size=0.15, random_state=42)
    X_val_DECODER, X_te_DECODER, y_val_DECODER, y_te_DECODER = train_test_split(X_te_DECODER, y_te_DECODER, test_size=0.5, random_state=42)
    
    X_tr_AUGMENTED, X_te_AUGMENTED, y_tr_AUGMENTED, y_te_AUGMENTED = train_test_split(list_tokens_augmented, summaries_augmented, test_size=0.15, random_state=42)
    X_val_AUGMENTED, X_te_AUGMENTED, y_val_AUGMENTED, y_te_AUGMENTED = train_test_split(X_te_AUGMENTED, y_te_AUGMENTED, test_size=0.5, random_state=42)
    
    X_tr = X_tr_DECODER + X_tr_AUGMENTED
    X_val = X_val_DECODER + X_val_AUGMENTED
    X_te = X_te_DECODER + X_te_AUGMENTED
    y_tr = y_tr_DECODER + y_tr_AUGMENTED
    y_val = y_val_DECODER + y_val_AUGMENTED
    y_te = y_te_DECODER + y_te_AUGMENTED

    #Apply feature extraction
    max_len_text = 200
    max_len_summary = 15

    X_tr, X_val, X_te, y_tr, y_val, y_te, x_tokenizer, y_tokenizer  = feature_extraction_training_augmented(max_len_text, max_len_summary, X_tr,y_tr, X_val,y_val,X_te, y_te, args.language)
    
if args.dataset == 'use_case':
    from src.transformers.imports.training_utils import feature_extraction_training

    filenames = code_files1 + code_files2
    filenames_tr, filenames_te = train_test_split(filenames, test_size=0.2, random_state=42)
    filenames_val, filenames_te = train_test_split(filenames_te, test_size=0.5, random_state=42)
    
    max_len_text = 200
    max_len_summary = 15

    X_tr, X_val, X_te, y_tr, y_val, y_te, x_tokenizer, y_tokenizer  = feature_extraction_training(max_len_text, max_len_summary, list_tokens_decoder, summaries_decoder, args.language)

token2idx = x_tokenizer.word_index
idx2token = x_tokenizer.index_word 
reverse_target_word_index=y_tokenizer.index_word 
reverse_source_word_index= idx2token 
target_word_index=y_tokenizer.word_index

current_dir = os.getcwd()
path_dir = 'transformer_best_model'
if not os.path.isdir(current_dir +'/' + path_dir):
    os.mkdir(current_dir +'/' + path_dir)

#Saving model dicts
dicts_directory = 'dicts'
if not os.path.isdir(current_dir + '/' + path_dir + '/' + dicts_directory):
    os.mkdir(current_dir + '/' + path_dir + '/' + dicts_directory)

import json
with open(path_dir + '/' + dicts_directory + "/token2idx_HyperOpt_transformer_"+str(args.language)+".json", "wb") as f:
    f.write(json.dumps(token2idx).encode("utf-8"))
    
with open(path_dir + '/' + dicts_directory + "/y2idx_HyperOpt_transformer_"+str(args.language)+".json", "wb") as f:
    f.write(json.dumps(target_word_index).encode("utf-8"))
    
with open(path_dir + '/' + dicts_directory + "/idx2token_HyperOpt_transformer_"+str(args.language)+".json", "wb") as f:
    f.write(json.dumps(idx2token).encode("utf-8"))
    
with open(path_dir + '/' + dicts_directory + "/idx2y_HyperOpt_transformer_"+str(args.language)+".json", "wb") as f:
    f.write(json.dumps(reverse_target_word_index).encode("utf-8"))

#Import utils
def create_masks(inp, tar):
    enc_padding_mask = create_padding_mask(inp)
    dec_padding_mask = create_padding_mask(inp)

    look_ahead_mask = create_look_ahead_mask(tf.shape(tar)[1])
    dec_target_padding_mask = create_padding_mask(tar)
    combined_mask = tf.maximum(dec_target_padding_mask, look_ahead_mask)

    return enc_padding_mask, combined_mask, dec_padding_mask

#Define Search Space
#search_space = {
#    'emb_dim': hp.choice('emb_dim', [512, 1024]),
#    'batch_size': hp.choice('batch size', [128,256]),
#    'dff_dim': hp.choice('dff_dim', [1024]),
#    'heads': hp.choice('heads', [4,8]),
#    'layers': hp.choice('layers',[1,2]), 
#    'dropout': hp.choice('dropout', [0.2, 0.3]),   
#    'warmup_steps': hp.choice('warmup_steps', [1000,2000, 4000])
#    #'lr': hp.uniform('lr',0.0001,0.001),
#    #"optimizer": hp.choice('optmz',["sgd", "rms"])
#}

#define this to save model at the end of the optimization
#warmup_steps = [1000,2000, 4000]
#emb_dim = [512, 1024]

#Define Search Space
search_space = {
    'emb_dim': hp.choice('emb_dim', [512]),
    'batch_size': hp.choice('batch size', [256]),
    'dff_dim': hp.choice('dff_dim', [1024]),
    'heads': hp.choice('heads', [4]),
    'layers': hp.choice('layers',[2]), 
    'dropout': hp.choice('dropout', [0.2]),   
    'warmup_steps': hp.choice('warmup_steps', [4000])
    #'lr': hp.uniform('lr',0.0001,0.001),
    #"optimizer": hp.choice('optmz',["sgd", "rms"])
}

#define this to save model at the end of the optimization
warmup_steps = [4000]
emb_dim = [512]

#Define objective function where in each trials the predictions and BLEU_4 values are computed for the model
def objective(params):
    """
    This method is called for each combination of parameter set to train the model and validate it against validation data
    to see all the results, from which best can be selected.
    """
    print("\nTrying params:",params)
    
    BUFFER_SIZE = 5000
    BATCH_SIZE = params['batch_size']
    EPOCHS = 500

    N = 7
    my_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N))

    dataset = tf.data.Dataset.from_tensor_slices((X_tr, y_tr)).shuffle(BUFFER_SIZE).batch(BATCH_SIZE)
    dataset_val = tf.data.Dataset.from_tensor_slices((X_val, y_val)).shuffle(BUFFER_SIZE).batch(BATCH_SIZE)
    dataset_test = tf.data.Dataset.from_tensor_slices((X_te, y_te)).shuffle(BUFFER_SIZE).batch(BATCH_SIZE)

    # hyper-params
    num_layers = params['layers']
    d_model = params['emb_dim']
    dff = params['dff_dim']
    num_heads = params['heads']

    encoder_maxlen = 200
    decoder_maxlen = 15  
    encoder_vocab_size, decoder_vocab_size = len(token2idx), len(y_tokenizer.word_index)
    
    transformer = Transformer(
    num_layers, 
    d_model, 
    num_heads, 
    dff,
    encoder_vocab_size, 
    decoder_vocab_size, 
    pe_input=encoder_vocab_size, 
    pe_target=decoder_vocab_size,
    rate = params['dropout']
)
    learning_rate = CustomSchedule(d_model, params['warmup_steps'])
    optimizer = tf.keras.optimizers.Adam(learning_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-9)
    loss_object = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True, reduction='none')

    def loss_function(real, pred):
        mask = tf.math.logical_not(tf.math.equal(real, 0))
        loss_ = loss_object(real, pred)

        mask = tf.cast(mask, dtype=loss_.dtype)
        loss_ *= mask

        return tf.reduce_sum(loss_)/tf.reduce_sum(mask)

    train_loss = tf.keras.metrics.Mean(name='train_loss')
    val_loss = tf.keras.metrics.Mean(name='val_loss')

    train_step_signature = [
    tf.TensorSpec(shape=(None, None), dtype=tf.int32),
    tf.TensorSpec(shape=(None, None), dtype=tf.int32)]

    @tf.function(input_signature=train_step_signature)
    def train_step(inp, tar):
        tar_inp = tar[:, :-1]
        tar_real = tar[:, 1:]

        enc_padding_mask, combined_mask, dec_padding_mask = create_masks(inp, tar_inp)
        all_inputs = [inp, tar_inp, enc_padding_mask, combined_mask, dec_padding_mask]

        with tf.GradientTape() as tape:
            predictions, _ = transformer(
                all_inputs, True
            )
            loss = loss_function(tar_real, predictions)

        gradients = tape.gradient(loss, transformer.trainable_variables)    
        optimizer.apply_gradients(zip(gradients, transformer.trainable_variables))

        train_loss(loss)

    def test_step(inp, tar):
        tar_inp = tar[:, :-1]
        tar_real = tar[:, 1:]

        enc_padding_mask, combined_mask, dec_padding_mask = create_masks(inp, tar_inp)
        all_inputs = [inp, tar_inp, enc_padding_mask, combined_mask, dec_padding_mask]

        predictions, _ = transformer(
                all_inputs, True
            )
        loss = loss_function(tar_real, predictions)
        test_loss(loss)

    checkpoint_path = "hyperopt_trials/ckpt_"+ my_string
    ckpt = tf.train.Checkpoint(optimizer=optimizer, model = transformer)
    #ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=10, checkpoint_name='transformer_'+str(learning_rate))

    #if ckpt_manager.latest_checkpoint:
    #    ckpt.restore(ckpt_manager.latest_checkpoint)
    #    print ('Latest checkpoint restored!!')
        
    # Define our metrics
    train_loss = tf.keras.metrics.Mean('train_loss', dtype=tf.float32)
    #train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy('train_accuracy')
    test_loss = tf.keras.metrics.Mean('test_loss', dtype=tf.float32)
    #test_accuracy = tf.keras.metrics.SparseCategoricalAccuracy('test_accuracy')

    current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    train_log_dir = 'hyperopt_trials/transformer_'+ my_string + '/' + current_time + '/training'
    test_log_dir = 'hyperopt_trials/transformer_'+ my_string + '/' + current_time + '/validation'
    lr_log_dir = 'hyperopt_trials/transformer_' + my_string + '/' + current_time + '/learning_rate'
    train_summary_writer = tf.summary.create_file_writer(train_log_dir)
    test_summary_writer = tf.summary.create_file_writer(test_log_dir)
    lr_summary_writer = tf.summary.create_file_writer(lr_log_dir)


    val_loss_history = []
    train_loss_history = []
    patience = 10
    for epoch in range(EPOCHS):

        start = time.time()
        train_loss.reset_states()
        val_loss.reset_states()

        for (batch, (inp, tar)) in enumerate(dataset):
            train_step(inp, tar)

            with train_summary_writer.as_default():
                 tf.summary.scalar('loss', train_loss.result(), step=epoch)
                 #tf.summary.scalar('accuracy', train_accuracy.result(), step=epoch)

        for (batch_t,(x_test, y_test)) in enumerate(dataset_val):
            test_step(x_test, y_test)
            with test_summary_writer.as_default():
                tf.summary.scalar('loss', test_loss.result(), step=epoch)
                #tf.summary.scalar('accuracy', test_accuracy.result(), step=epoch)

        if (epoch + 1) % 1 == 0:
            ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=patience+1, checkpoint_name='transformer_loss_'+str(test_loss.result().numpy().round(6))+'_')

            ckpt_save_path = ckpt_manager.save()
            print ('Saving checkpoint for epoch {} at {}'.format(epoch+1, ckpt_save_path))

        print ('Epoch {} - Training Loss {:.4f}'.format(epoch + 1, train_loss.result()))
        print('Epoch {} - Validation Loss {:.4f}'.format(epoch + 1, test_loss.result()))
        print ('Time taken for 1 epoch: {} secs\n'.format(time.time() - start))

        with lr_summary_writer.as_default():
            tf.summary.scalar('learning_rate', optimizer._decayed_lr(tf.float32), step=epoch)

        train_loss_history.append(train_loss.result().numpy())
        val_loss_history.append(test_loss.result().numpy())
        
        #print(val_loss_history)
        #print(min(val_loss_history))
        min_value = min(val_loss_history)
        min_index = val_loss_history.index(min_value)
        #print(min_index)
        
        stopEarly = Callback_EarlyStopping(val_loss_history, min_delta=0.0005, patience=10)
        if stopEarly:
            #Search for min loss epoch and save only that checkpoint
            print('Retrieving min loss epoch')
            checkpoint_NAME = 'transformer_loss_'+str(min(val_loss_history).round(6))+'_'
            ckpt.restore( "hyperopt_trials/ckpt_"+ my_string +'/'+checkpoint_NAME+'-'+str(min_index+1))
            ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=1, checkpoint_name='transformer_best_on_trial')
            ckpt_save_path = ckpt_manager.save()
            break  
    
    if stopEarly == False:
        ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=1, checkpoint_name='transformer_best_on_trial')
        ckpt_save_path = ckpt_manager.save()
            
                        

    encoder_maxlen = 200 
    decoder_maxlen = 15 
    encoder_vocab_size, decoder_vocab_size = len(token2idx), len(y_tokenizer.word_index)

    decoded_valid, target_valid = make_predictions(X_val, y_val, transformer,target_word_index, reverse_target_word_index, token2idx, reverse_source_word_index, encoder_maxlen, decoder_maxlen)
    bleu_1, bleu_2, bleu_3, bleu_4_val = get_bleu_score(decoded_valid, target_valid)
    print(np.mean(bleu_1))
    print(np.mean(bleu_2))
    print(np.mean(bleu_3))
    print(np.mean(bleu_4_val))
    
    predictions = pd.DataFrame()
    predictions['prediction'] = decoded_valid
    predictions['ground_truth'] = target_valid
    predictions['filename'] = filenames_valid
    predictions.to_csv(path_dir + '/valid_predictions.csv')
    transformer.save('transformer_best_model/model', save_format = 'tf')
        
    return {'loss': -np.mean(bleu_4_val), 'status': STATUS_OK, 'model': transformer, 'valid_bleu': np.mean(bleu_4_val), 'actual_loss':test_loss.result()} 
    

#Launch optimization   
trials = Trials()
best = fmin(fn=objective , space=search_space, algo=tpe.rand.suggest, max_evals=iterations, trials=trials)
print ('Best selected model: ')
print (best)
#save trial object
#pickle.dump(trials, open("hyperopt_trials/trained_trials.p", "wb"))

def getBestModelfromTrials(trials):
    valid_trial_list = [trial for trial in trials
                            if STATUS_OK == trial['result']['status']]
    losses = [float(trial['result']['loss']) for trial in valid_trial_list]
    index_having_minumum_loss = np.argmin(losses)
    best_trial_obj = valid_trial_list[index_having_minumum_loss]
    
    return best_trial_obj['result']['model']  

def GetParametersFromTrials(trials):
    
    import hyperopt
    hypeList = []
    hypeLoss = []
    
    for trial in trials.trials:
        
        for_eval = {}
        #print(trial)
        for key, value in trial["misc"]["vals"].items():
            #print(key, value[0])
            if len(str(value)) > 2:
                for_eval[key] = value[0]
            
        hypeList.append(hyperopt.space_eval(search_space, for_eval))
    
    hypeLoss = [t['result']['actual_loss'] for t in trials.trials]
    Bleu = [t['result']['valid_bleu'] for t in trials.trials]
    
    return hypeList, hypeLoss, Bleu

#Save best model
best_model = getBestModelfromTrials(trials)
checkpoint_path = path_dir
learning_rate = CustomSchedule(emb_dim[best['emb_dim']], warmup_steps[best['warmup_steps']])
optimizer = tf.keras.optimizers.Adam(learning_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-9)
ckpt = tf.train.Checkpoint(optimizer=optimizer, model = best_model)
ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=1, checkpoint_name=path_dir)
ckpt_save_path = ckpt_manager.save()

best_model.save('transformer_best_model/my_best_model', save_format = 'tf')
#loaded_model = tf.keras.models.load_model('transformer_best_model/my_model', custom_objects={'Transformer':Transformer, 'Encoder': Encoder, 'Decoder': Decoder, 'MultiHeadAttention': MultiHeadAttention})

#Save optimization parameters table
parameters, loss, Bleu = GetParametersFromTrials(trials)  
paramsDF = pd.DataFrame(parameters)
paramsDF['val_loss'] = loss
paramsDF['valid_bleu4'] = Bleu
print(paramsDF)
paramsDF.to_csv(path_dir + '/hyperopt_lr_params.csv', index=False)

#Predicting with best model
encoder_maxlen = 200 
decoder_maxlen = 15  
encoder_vocab_size, decoder_vocab_size = len(token2idx), len(y_tokenizer.word_index)

decoded_train, target_train = make_predictions(X_tr, y_tr, best_model,target_word_index, reverse_target_word_index, token2idx, reverse_source_word_index, encoder_maxlen, decoder_maxlen)
predictions = pd.DataFrame()
predictions['prediction'] = decoded_train
predictions['ground_truth'] = target_train
predictions['filename'] = filenames_tr
predictions.to_csv(path_dir + '/train_predictions.csv')

print('Training Set:')
bleu_1, bleu_2, bleu_3, bleu_4 = get_bleu_score(decoded_train, target_train)
print(np.mean(bleu_1))
print(np.mean(bleu_2))
print(np.mean(bleu_3))
print(np.mean(bleu_4))

decoded_valid, target_valid = make_predictions(X_val, y_val, best_model,target_word_index, reverse_target_word_index, token2idx, reverse_source_word_index, encoder_maxlen, decoder_maxlen)
predictions = pd.DataFrame()
predictions['prediction'] = decoded_valid
predictions['ground_truth'] = target_valid
predictions['filename'] = filenames_val
predictions.to_csv(path_dir + '/valid_predictions.csv')

print('Validation Set:')
bleu_1, bleu_2, bleu_3, bleu_4 = get_bleu_score(decoded_valid, target_valid)
print(np.mean(bleu_1))
print(np.mean(bleu_2))
print(np.mean(bleu_3))
print(np.mean(bleu_4))


decoded_test, target_test = make_predictions(X_te, y_te, best_model, target_word_index, reverse_target_word_index, token2idx, reverse_source_word_index, encoder_maxlen, decoder_maxlen)
predictions = pd.DataFrame()
predictions['prediction'] = decoded_test
predictions['ground_truth'] = target_test
predictions['filename'] = filenames_te
predictions.to_csv(path_dir + '/test_predictions.csv')

print('Test Set:')
bleu_1, bleu_2, bleu_3, bleu_4 = get_bleu_score(decoded_test, target_test)
print(np.mean(bleu_1))
print(np.mean(bleu_2))
print(np.mean(bleu_3))
print(np.mean(bleu_4))


# Upload results to s3
#print('Sending content of folder {} to S3...'.format(path_dir))
#new_s3_folder = upload_folder_to_s3(path_dir)
#print('Files successfully uploaded! You can find them here: {}'.format(new_s3_folder))
#print('Please remember you have to be logged into the console: https://treelogic-decoder.signin.aws.amazon.com/console')

