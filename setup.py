from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Machine Learning Model. Extraction of information from source code and the automatic generation of summaries to describe the goal of the different methods.',
    author='TREE TECHNOLOGY S.A.',
    license='',
)
